<?php


/*REQUIRE AUTHENTICATED USER
-------------------------------------------*/
//submit new comment by POST method
Route::post('/posts/{post}', 'CommentsController@store')->middleware('auth');




/*ADMIN only routes
/------------------------------------------------*/
//view form  to create new post
Route::get('/posts/create', 'PostsController@create')->middleware('admin');
//submit new post by POST
Route::post('/posts', 'PostsController@store')->middleware('admin');
Route::get('/posts/{post}/edit', 'PostsController@edit')->middleware('admin');
Route::patch('/posts', 'PostsController@update')->middleware('admin');




/*UNAUTHENTICATED ROUTES
----------------------------------------*/
Route::get('/', 'PostsController@home');

//view list of posts
Route::get('/posts', 'PostsController@index');

//view post details
Route::get('posts/{post}', 'PostsController@show');

Route::get('/archives/{year}/{month}', 'PostsController@archive');





//this is created automatically when do 'php artisan make::auth' in tinker
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');





@extends('layouts.master')



@section('content')
        
        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">
            All Posts for {{Ucfirst($month) . ' ' . $year}}.<!--page heading, Uppercase first letter of the month-->
          </h1>
          
          
          @foreach($posts as $post)
          <!-- Blog Post -->
          <div class="card mb-4">
            <img class="card-img-top" src="/images/{{ $post->thumbnail_image }}" alt="{{ $post->title }}">
            <div class="card-body">
              <h2 class="card-title">{{ $post->title }}</h2>
              <p class="card-text">{{ $post->body }}</p>
              <a href="/posts/{{$post->id}}" class="btn btn-primary">Read More &rarr;</a>
            </div>
            <div class="card-footer text-muted">
              Posted on {{$post->created_at->toFormattedDateString()}} by <!-- {{$post->created_at}} -->
              <a href="#">{{$post->user()->name}}</a>
            </div>
          </div>
          @endforeach
          
          
          <!-- Pagination    links to page numbers-->
          @include('layouts.partials.paginate')

        </div>

        

@endsection

















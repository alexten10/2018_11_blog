@extends('layouts.master')


@section('content')

        <!-- blog entries column -->
        <div class="col-md-8">

          <!-- Title -->
          <h1 class="mt-4">{{ $post->title }}</h1>

          <!-- Author -->
          <p class="lead">
            by
            <a href="#">{{$post->user()->name}}</a>
          </p>
          <hr>

          <!-- Date/Time -->
          <p>Posted on {{$post->created_at}}</p><!--can add ->toFormattedDateString() -->
          <hr>

          <!-- Preview Image -->
          <img class="img-fluid rounded" src="/images/{{ $post->thumbnail_image }}" alt="{{ $post->title }}" width="900" height="300">
          <hr>

          <!-- Post Content -->
          <p class="lead">{{ $post->body }}</p>
          <hr>
          
          
          <!-- if logged in and if user is admin, show link to edit the post -->
          @if( Auth::user() && Auth::user()->isAdmin ) <!-- can use  Auth:check() instead user() -->
            <a href="/posts/{{$post->id}}/edit">Edit</a>
          @endif


          <!-- flash message on successful adding a comment-->
          <!--check if session 'message' comes from store() in CommentsController.php -->
          @if(session()->has('message'))
            <div class="alert alert-success">
              {{ session()->get('message') }}
            </div>
          @endif

          <!--if logged in (if) show text box to leave a comment, if not logged in show a message (else)-->
          @if( Auth::check() )
            <!-- Comments Form -->
            <div class="card my-4">
              <h5 class="card-header">Leave a Comment:</h5>
              <div class="card-body">
                <form method="post" action="/posts/{{ $post->id }}">
                  @include('layouts.partials.errors') <!--show errors of validation-->
                  {{ csrf_field() }} <!--without this line, will be error 419 after pressing submit button-->
                  <input type="hidden" name="id_of_user" value="{{ Auth::getUser()->id }}" /> <!-- value = get logged in user id. alternative code: value="Auth::id()}}"-->
                  <input type="hidden" name="id_of_post" value="{{ $post->id }}" />
                  <div class="form-group">
                    <textarea class="form-control" rows="3" name="comment" id="comment">{{ old('body') }}</textarea>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </form>
              </div>
            </div>
          @else
            <h2>Please, <a href="/login">log in</a> or <a href="/register">register</a> to leave a comment</h2>
            <hr />
          @endif

          <!--comments area -->
          <!--show number of comments -->
          <h3>Comments ({{ $post->comments()->get()->count() }})</h3><br />
          <!-- if no comments, print a message -->
          @if($post->comments()->get()->isEmpty())
            <div class="media mb-4">
              <div class="media-body">
                <i>No comments yet</i>
              </div>
            </div>
          
          <!-- if comments exist, show all comments for the post -->
          @else
            @foreach($post->comments()->get() as $comment)
            <!-- Single Comment -->
            <div class="media mb-4">
              <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
              <div class="media-body">
                <h5 class="mt-0">{{ $comment->user()->name }}</h5> <!-- getting comment's user name -->
                {{ $comment->body }} <!-- getting comment body -->
              </div>
            </div>
            @endforeach
          @endif


        </div><!--end .col-md-8-->

@endsection














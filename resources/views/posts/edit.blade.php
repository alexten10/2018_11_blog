@extends('layouts.master')



@section('content')
        
        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">Edit post</h1>
            
            {!! Form::model($post, ['url' =>'/posts', 'method' => 'PATCH']) !!}
            
            
            @include('layouts.partials.errors')
            
              <div class="form-group">
                {!! Form::label('title', 'Post Title') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
                {!! Form::hidden('id', $post->id) !!}
              </div>
              
              
              <div class="form-group">
                {!! Form::label('featured_image', 'Featured Image') !!}
                {!! Form::text('featured_image', null, ['class' => 'form-control']) !!}<!-- name of text area=body, value=null-->
              </div>
              
              
              <div class="form-group">
                {!! Form::label('thumbnail_image', 'Thumbnail Image') !!}
                {!! Form::text('thumbnail_image', null, ['class' => 'form-control']) !!}<!-- name of text area=body, value=null-->
              </div>
              
              
              <div class="form-group">
                {!! Form::label('body', 'Post Content') !!}
                {!! Form::textarea('body', null, ['class' => 'form-control']) !!}<!-- name of text area=body, value=null-->
              </div>
              
              
              <div class="form-group">
                {!! Form::submit('Update', ['class' => 'btn btn-primary']); !!}
              </div>
              
            {!! Form::close() !!}
            
            
            
            
            
          </form>
          
          
          
        </div><!-- END .col-md-8 -->
        
@endsection

















@extends('layouts.master')



@section('content')
        
        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">Edit post</h1>
          
          <!-- if a user is logged in, show "you are logged in"-->
          @if( Auth::check() )
          <h2>you are logged in</h2>
          @endif
          
          
          
          <form method="post" action="/posts">
            <input type="hidden" name="_method" value="PATCH" />
            
            
            @include('layouts.partials.errors')
            
            {{ csrf_field() }} <!-- Cross-Site Request Forgery-->
            
            <input type="hidden" name="id" value="{{$post->id}}" />
            
            <div class="form-group">
              <label for="title">Title</label>
              <!--must have name; value="{{old('title') }}" for sticky form, if not passed validation-->
              <input type="text" class="form-control" id="title" name="title" value="{{ old('title', $post->title) }}">
            </div>
            
            <div class="form-group">
              <label for="body">Body</label>
              <textarea class="form-control" name="body" id="body">{{ old('body', $post->body) }}</textarea><!--{{old('body') }} for sticky form-->
            </div>
            
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Publish</button>
            </div>
            
          </form>
          
          
          
        </div><!-- END .col-md-8 -->
        
@endsection

















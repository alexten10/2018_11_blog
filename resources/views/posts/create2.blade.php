@extends('layouts.master')



@section('content')
        
        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">Create New Post</h1>
          
          {!! Form::open(['url' => '/posts']) !!}
            
            <div class="form-group">
              {!! Form::label('title', 'Post Title') !!}
              {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>
            
            
            <div class="form-group">
              {!! Form::label('featured_image', 'Featured Image') !!}
              {!! Form::text('featured_image', null, ['class' => 'form-control']) !!}<!-- name of text area=body, value=null-->
            </div>
            
            
            <div class="form-group">
              {!! Form::label('thumbnail_image', 'Thumbnail Image') !!}
              {!! Form::text('thumbnail_image', null, ['class' => 'form-control']) !!}<!-- name of text area=body, value=null-->
            </div>
            
            
            <div class="form-group">
              {!! Form::label('body', 'Post Content') !!}
              {!! Form::textarea('body', null, ['class' => 'form-control']) !!}<!-- name of text area=body, value=null-->
            </div>
            
            
            <div class="form-group">
              {!! Form::submit('Publish', ['class' => 'btn btn-primary']); !!}
            </div>
            
          {!! Form::close() !!}
          
          
        </div><!-- END .col-md-8 -->
        
@endsection

















@extends('layouts.master_home')

@section('content')

      <!-- Jumbotron Header -->
      <header class="jumbotron my-4">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          
          <ol class="carousel-indicators">
            
            <!-- with foreach DYNAMIC ++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
            @foreach($posts as $i => $post)
              @if ($i==0)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="active"></li>
              @else
                <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}"></li>
              @endif
            @endforeach
            
            <!-- without foreach HARDCODED ++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
            <!--
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
          -->
          
          </ol>
          
          
          
          <div class="carousel-inner">
            
            <!--  SOLUTION #1 *****************************************************************************************-->
            @foreach ($posts as $i => $post)
              @if ($i==0)
                <div class="carousel-item active">
              @else
                <div class="carousel-item">
              @endif
                <a href="/posts/{{ $post->id }}"><img class="d-block w-100" src="images/{{ $post->featured_image }}?auto=yes" alt="{{ $post->title }}" /></a>
              </div>
            @endforeach
            
            <!-- SOLUTION #2*************************************************************************************** -->
            <!--
            <div class="carousel-item active">
              <a href="/posts/{{ $posts[0]->id }}"><img class="d-block w-100" src="images/{{ $posts[0]->featured_image }}?auto=yes&bg=777&fg=555&text=First slide" alt="First slide" /></a>
            </div>
            <div class="carousel-item">
              <a href="/posts/{{ $posts[1]->id }}"><img class="d-block w-100" src="images/{{ $posts[1]->featured_image }}?auto=yes&bg=666&fg=444&text=Second slide" alt="Second slide"></a>
            </div>
            <div class="carousel-item">
              <a href="/posts/{{ $posts[2]->id }}"><img class="d-block w-100" src="images/{{ $posts[2]->featured_image }}?auto=yes&bg=555&fg=333&text=Third slide" alt="Third slide"></a>
            </div>
            <div class="carousel-item">
              <a href="/posts/{{ $posts[3]->id }}"><img class="d-block w-100" src="images/{{ $posts[3]->featured_image }}?auto=yes&bg=555&fg=333&text=Fourth slide" alt="Fourth slide"></a>
            </div>
            -->
            
          </div><!-- END class="carousel-inner"-->
          
          
          
          
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          
        </div>
      </header>


      <!-- Page Features -->
      <div class="row text-center">

        @foreach ($posts as $post)
        <div class="col-lg-3 col-md-6 mb-4">
          <div class="card">
            <img class="card-img-top" src="images/{{ $post->thumbnail_image }}" alt="">
            <div class="card-body">
              <h4 class="card-title">{{ $post->title }}</h4>
              <p class="card-text">{{ $post->excerpt }}</p>
            </div>
            <div class="card-footer">
              <a href="/posts/{{ $post->id }}" class="btn btn-primary">Find Out More!</a>
            </div>
          </div>
        </div>
        @endforeach

      </div>
      <!-- /.row -->
@endsection









<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if( !\Auth::check() || !\Auth::user()->isAdmin)
      {
        //set flash message
        session()->flash('flash', 'you must be an admin user');
        return redirect ('/');
      }
      return $next($request);
    }
}

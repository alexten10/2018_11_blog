<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostsController extends Controller
{

  public function home()
  {
    $posts = Post::latest()->limit(4)->get(); //send 4 latest posts to the home page
    //dd ($posts);
    return view('welcome', compact('posts'));
  }


  /**
   * get all posts
   * @return array with results
   */
  public function index()
  {
    //$posts = Post::latest() will order by creation order
    //$posts = Post::oldest('id')  //order by asc id, starting from id=1
    $posts = Post::latest('id')  //order by desc id
                   ->with('categories')
                   ->Paginate(10); //display 10 posts per page
    return view('posts.index', compact('posts'));
  }


  public function archive($year, $month)
  {
    $posts = Post::filter(['year' => $year, 'month' => $month])->paginate(10);
    return view('posts.archive', compact('posts','year','month'));
  }



  /**
   * get post details
   * $post - int(post_id)
   * @return array with results
   */
  public function show(Post $post)
  {
    return view('posts.show', compact('post'));
  }



/**
 * if function is called, route to posts/create page
 */
  public function create()
  {
    return view('posts.create2');
  }


/**
 * validate input and insert it into table
 * @return redirect to /posts page
 */
  public function store()
  {
    //dd(request()->all());
    
    //validate fields, if errors occur returns errors to create.blade.php
    $this->validate(request(), [
      'title' => 'required|min:2|max:255',
      'body' => 'required|min:20'
    ]);
    
    Post::create([
      'title' => request('title'),
      'body' => request('body')
    ]);
    
    return redirect('/posts');
  }


  /*
  public function store(Request $request)
  {
    $request->validate([]);
    
    //validate fields
    $this->validate(request(), [
      'title' => 'required|min:2|max:255',
      'body' => 'required|min:20'
    ]);
    
    //same as above
    Post::create([
      'title' => request('title'),
      'body' => request('body')
    ]);
    
    return redirect('/posts');
  }
  */


  /**
   * get post info by id and send with redirection to /posts/edit page
   * @return $post array with results
   */
  public function edit (Post $post)
  {
    //dd($post);
    return view('posts.edit', compact('post'));
  }




    /**
   * request data from input fields, validate it, if ok update database
   * redirect to the /posts/post_id page (detailed view of the post)
   */
  public function update()
  {
    //dd('patch request');
    
    //validate input data
    $this->validate(request(),[
      'title' => 'required',
      'body' => 'required',
      'id' => 'required'
    ]);
    
    //dd(request(['id', 'title', 'body']));
    
    //find the post by id and save new data into database
    $post = Post::find(request('id'));
    $post->title = request('title');
    $post->body = request('body');
    $post->save();
    return redirect('/posts/' . $post->id); //redirect to the page with post
  }




}

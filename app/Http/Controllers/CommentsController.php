<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentsController extends Controller
{
  
  /**
   * validate input and if success add into the DB table
   * @return redirect back with successful message
   */
  public function store()
  {
    //dd(request()->all()); //for checking purposes
    
    //validate field named "comment" for min 1 char and max 2000 chars, if errors occur return errors
    $this->validate(request(), [
      'comment' => 'required|min:1|max:2000'
    ]);
    
    //insert in comments table into user_id column value of 'id_of_user' input field, etc
    //Route::post('/posts/{post}', 'CommentsController@store'); in web.php
    Comment::create([
      'user_id' => request('id_of_user'),
      'post_id' => request('id_of_post'),
      'body' => htmlspecialchars(strip_tags(request('comment')))//del html tags and convert special chars
    ]);
  
    //redirect back with session 'message' = 'comment added'
    return redirect()->back()->with('message', 'comment added');
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;
use App\Post;
use App\Category;
use Carbon\Carbon;

class Post extends Model
{
  protected $fillable = ['title','body'];
  //protected $guarded = [];



  //$post->comments()
  public function comments()
  {
    return $this->hasMany('App\Comment');
  }


  //$post->user()
  public function user()
  {
    return $this->belongsTo('App\User')->first();
  }



  static public function archives()
  {
    return Post::selectRaw('year(created_at) as year, monthname(created_at) as month, count(*) as num')
                                ->groupBy('year', 'month')
                                ->orderByRaw('min(created_at) desc')
                                ->get()
                                ->toArray();
  }



  public function scopeFilter($query, $params)
  {
    //convert month string to number
    $month = Carbon::parse($params['month'])->month;
    
    //return posts for year and month
    return Post::latest()->whereYear('created_at', $params['year'])
                         ->whereMonth('created_at', $month);
  }
  
  
  public function categories()
  {
    return $this->belongsToMany(Category::class); //same as $this->belongsToMany('App\Category');
    
  }

}

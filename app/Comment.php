<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class Comment extends Model
{
  
  protected $fillable = ['user_id','post_id', 'body'];
  
  
  //$comment->post()
  public function post()
  {
    return $this->belongsTo('App\Post');
  }
  
  
  public function user()
  {
    return $this->belongsTo('App\User')->first();//the first() method returns the first result of a result set
  }
  
  
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;


class Category extends Model
{
  
  public function posts()
  {
    return $this->belongsToMany(Post::class); //same as $this->belongsToMany('App\Post');
    
  }
  
  public static function categories()
  {
    Category::selectRaw('')->get()->toArray();
  }
  
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      //provide archves to sidebar
      //layouts.partials.sidebar must be written the same exact syntax as include , not layouts/partials/sidebar in this case
      //2  ->with -- return 2 different sections for 2 sidebar's blocks
      view()->composer('layouts.partials.sidebar', function($view) {
        $view->with('archives', \App\Post::archives())->with('categories', \App\Category::all());
      });
    }

//with('category', \App\Category::all())
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

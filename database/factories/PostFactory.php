<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
      'title' => $faker->sentence,
      'body' => $faker->realText,
      'featured_image' => 'image.img',
      'thumbnail_image' => 'thumb.img',
      'created_at' => $faker->dateTimeThisYear,
      'updated_at' => Carbon::now()
    ];
});

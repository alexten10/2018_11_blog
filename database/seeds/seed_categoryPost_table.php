<?php

use Illuminate\Database\Seeder;

class seed_categoryPost_table extends Seeder
{
  
  //php artisan db:seed --class=seed_categoryPost_table
  // to seed only 1 specific table
  
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('category_post')->insert(
        ['post_id' => 1, 'category_id' =>1]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 1, 'category_id' =>2]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 1, 'category_id' =>3]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 2, 'category_id' =>4]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 2, 'category_id' =>5]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 3, 'category_id' =>1]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 3, 'category_id' =>2]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 4, 'category_id' =>2]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 4, 'category_id' =>4]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 5, 'category_id' =>1]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 5, 'category_id' =>2]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 5, 'category_id' =>3]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 5, 'category_id' =>4]
      );
      
      DB::table('category_post')->insert(
        ['post_id' => 5, 'category_id' =>5]
      );
      
      
    }
}

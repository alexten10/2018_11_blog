<?php

use Illuminate\Database\Seeder;

class seed_posts_table extends Seeder
{
  
  //php artisan db:seed --class=seed_posts_table
  // to seed only 1 specific table
  
  
    /**
     * Run the database seeds.
     * (insert into 'posts' table
     * @return void
     */
    public function run()
    {
      
      //post id=1
      DB::table('posts')->insert([
        'title' => 'Scientists have discovered a new way to use coffee grounds to help save the Earth',
        'excerpt' => 'Those damp, spent coffee grounds languishing in your compost bin may be more than just waste — they may actually help save the planet from global warming.',
        'user_id' => 1,
        'body' => "Those damp, spent coffee grounds languishing in your compost bin may be more than just waste — they may actually help save the planet from global warming.
                  That's the word from a paper published in the journal Nanotechnology, in which researchers from the Ulsan National Institute of Science and Technology in South Korea have figured out a way to soak and heat used coffee grounds, turning them into the grittiest, most delicious methane storage source ever.
                  Methane is the second most abundant greenhouse gas that humans are spewing into the atmosphere after carbon dioxide. During a 100-year period, it's about 25 times worse for climate change than CO2.
                  Methane pollution comes from a variety of natural sources, including our wetlands, natural gas systems, and livestock, but more than 60% of total methane emissions worldwide result from human activities like agriculture, industry, and waste management.
                  Greenhouse gases as a whole are the major contributor to climate change. In fact, president Obama is tackling this problem with new rules aiming to cut 32% of greenhouse gas emissions from fossil fuel-fired power plants by 2030.
                  The new technique is promising because our current methods of capturing methane from the air are dangerous, expensive, and heavy. Our traditional way of compressing methane gas into cylinders subjects workers to sudden explosions, fires, and toxic gases, if the cylinders are not handled and stored correctly.
                  And this new coffee technique may be a cheap, easy, and quick way to help take a bite out of the problem.
                  Carbon-based materials, like coffee grounds, are ideal candidates for methane capture and storage because they're light, cheap, and durable.
                  Researchers mixed 1/2 cup of used Kirkland brand dark roast Colombian coffee grounds with sodium hydroxide for 24 hours, and then heated the mixture to between 1,300 and 1,700 degrees Fahrenheit in a furnace, producing a stable material that's able to absorb methane from the air around it. This process took only a day — a fraction of the time it normally takes to produce methane-absorbing materials.
                  Then they measured how much methane the treated grounds were able absorb from the air, and found that they were able to absorb up to 7% of its weight in methane. While that's lower than other methane-absorbing materials this coffee mix is faster and cheaper to produce.
                  Christian Kemp, an author of the paper from Pohang University of Science and Technology in Korea, got the idea while discussing a different project with colleagues over a cup of coffee, according to a press release.
                  'We were sitting around drinking coffee and looked at the coffee grounds and thought, 'I wonder if we can use this for methane storage?'' he said in the press release.
                  The technique is extremely cheap. 'The waste material [used, ground coffee] is free compared to all the metals and expensive organic chemicals needed in other processes. In my opinion, this is a far easier way to go,' Kemp said in the press release.
                  It also may be used one day to capture methane for use as a clean energy fuel alternative to fossil fuels.
                  As if we needed more reasons to be thankful for coffee.",
        'featured_image' => 'featured_image1.jpg',
        'thumbnail_image' => 'thumbnail_image1.jpg',
        'created_at' => Carbon\Carbon::now()->subDays(10), //-10 days from current date
        'updated_at' => Carbon\Carbon::now()->subDays(9) //-9 days from current date
      ]);
        
        //post id=2
        DB::table('posts')->insert([
        'title' => 'A generation of coffee snobs – many youngsters now refuse to drink instant',
        'excerpt' => "Instant coffee is falling out of fashion as coffee drinkers become more accustomed to 'finer tastes'",
        'user_id' => 1,
        'body' => "Instant coffee is falling out of fashion as coffee drinkers become more accustomed to 'finer tastes'
                  Just one-in-five coffee drinkers are drinking instant coffee more than once a day, and this falls to 8pc for people in their early 20s, according to new figures.
                  Sales of instant coffee fell by 3pc last year to £782m, down from £806m in 2013, and the market is expected to continue declining to hit £700m in 2020.
                  Research company Mintel, which released the figures as part of its annual report into the UK coffee market, said that while instant coffee still dominates sales, the market has been in decline for some time.
                  Almost 30pc of people said they drank instant coffee more than once a day in 2014, but this has dropped to 21pc this year. 'For consumers with a high frequency of visits to coffee shops, switching back to standard instant coffee in the home may not be appealing,' said Douglas Faughnan, an analyst at Mintel specialising in food and drink.
                  Ground coffee and coffee bean volume sales have stagnated for a number of years, with sales values dropping by 1.1pc in 2014, although this was largely due to supermarket price wars and falling inflation, which pushed down the prices of packets and jars.
                  Sales of upmarket filtered coffee – used to make espresso, cappuccino or filter coffee – have increased substantially in recent years.
                  Nescafe remains the leading brand in the instant coffee market, with its Original and Gold brands accounting for 35pc of total instant coffee sales between them.
                  However, the merger last year of Douwe Egberts maker D.E Master Blenders with the coffee division of Kraft's Mondelez means the newly-formed Jacobs Douwe Egberts is a 'formidable competitor', said Mintel.
                  The newly-created company owns brands including Kenco, Carte Noire, Tassimo and Douwe Egberts, and now accounts for 34pc of the instant market.
                  Single-serve coffee pods have also disrupted the market.
                  Since Nespresso lost its patent in late 2013, a number of companies have launched compatible coffee pods. One-in-five consumers now owns a coffee pod machine, said the report.
                  Consumers like the convenience of only making a single serving, and cafetieres are being used less frequently.
                  The report suggested 21pc of coffee drinkers would be interested in having a readymade coffee delivered at home or at their desk, while 16pc would consider having freshly roasted ground coffee delivered to their home.
                  Stephen Rapoport, founder of online coffee delivery service Pact Coffee, said consumers are now more discerning in their tastes.
                  'Instant coffee is a pale imitation of the real thing. It doesn't taste great and gives practically no value back to coffee farmers. Once you've experienced the beautiful flavour and taste in a cup of good quality, freshly roasted coffee, people just don't want to settle for instant anymore,' he said.
                  Britons are also becoming more willing to try new types of coffee.
                  This includes cold brew, where coarse-ground beans are soaked in room-temperature water for around 12 hours to form a coffee concentrate that is then diluted with water and usually served chilled.
                  It differs from iced coffee, which is brewed like normal coffee and left to chill before being served over ice.
                  Cold brew, currently taken up by a handful of artisan coffee shops, could become more familiar across the UK after Starbucks launched its own version at the London Coffee Festival earlier this year.",
        'featured_image' => 'featured_image2.jpg',
        'thumbnail_image' => 'thumbnail_image2.jpg',
        'created_at' => Carbon\Carbon::now()->subDays(8),
        'updated_at' => Carbon\Carbon::now()->subDays(8)
      ]);
        
        //post id = 3
        DB::table('posts')->insert([
        'title' => 'Why Coffee Makes You Poop',
        'excerpt' => "Coffee increases contractions in your gut, which activate that gotta-go urge as stool travels to your rectum, says Satish Rao, M.D., Ph.D., the director of the digestive health center at Georgia Regents University.",
        'user_id' => 1,
        'body' => "If you're like a lot of guys, your morning cup of Joe leaves you bright-eyed and ready to take on the day—just as soon as you're done pooping. But what is it about the brew that blasts your bowels? At the surface, it's pretty simple: Coffee increases contractions in your gut, which activate that gotta-go urge as stool travels to your rectum, says Satish Rao, M.D., Ph.D., the director of the digestive health center at Georgia Regents University.
                  Two decades ago, Dr. Rao and his teams recruited 12 lucky people to wear anal probes with sensors that measured pressure activity throughout different parts of their colons and rectums. Over the course of 10 hours, the subjects drank the same amounts of caffeinated coffee, decaf coffee, and hot water, and ate a 1,000-calorie burger meal.
                  The food triggered the greatest activity in the participants' guts, but the researchers were surprised to discover that caffeinated java prompted contractions of a near-similar magnitude—60 percent stronger than that of hot water, and 23 percent more intense than decaf. 
                  This shows caffeine—a known stimulant—does play a role in jumpstarting your colon, but it's not the only player involved. There's likely something in coffee itself that's responsible for your need to go number 2, says Dr. Rao.
                  Experts have a theory: Just minutes after you ingest java, it reaches your stomach. One or more of its hundreds of compounds triggers the production of certain hormones in your body, such as motilin—which stimulates gut contractions—or gastrin, which causes the secretion of acid in your stomach. That's when the pooping kicks in.  
                  But scientists say it's also possible that a combination of everything involved in your A.M. routine sends you scurrying to the toilet.
                  Your colon is about twice as active in the morning thanks to your body's circadian rhythms, says Dr. Rao. Add in breakfast, wash it down with coffee—both of which independently spark colonic contractions—and you can see why you're clamoring for a crap afterward.  
                  To avoid spending overtime in the office restroom, hold off on making your Starbucks run until about 2 hours after you wake up. This will give your colon enough time to calm down from its morning rev, Dr. Rao says.
                  And when you drink your first cup, try not to pair it with food or exercise, since they'll both get your gut going, too.",
        'featured_image' => 'featured_image3.jpg',
        'thumbnail_image' => 'thumbnail_image3.jpg',
        'created_at' => Carbon\Carbon::now()->subDays(8),
        'updated_at' => Carbon\Carbon::now()->subDays(8)
      ]);
        
        //post id = 4
        DB::table('posts')->insert([
        'title' => "Get More Out of Your Coffee!",
        'excerpt' => "Caffeine: It makes you feel like a human in the morning, is a known performance-enhancer, and gives you the pick-me-up you need at 3 p.m. But in order to make the most out of your habit, there are specific ways to drink your joe.",
        'user_id' => 1,
        'body' => "Caffeine: It makes you feel like a human in the morning, is a known performance-enhancer, and gives you the pick-me-up you need at 3 p.m. But in order to make the most out of your habit, there are specific ways to drink your joe. And sometimes, a cup of coffee when you crave one isn't a good idea. Here's a look at how to give your brain and body a boost—while still being able to sleep at night.  Drink coffee within 30 minutes of a meeting—or right after
                  Your body absorbs about half of the caffeine in your cup in seven minutes, explains Frank Ritter, Ph.D. who developed Caffeine Zone—an app that helps you monitor your body's caffeine levels. And if you drank 200 milligrams (mg)—about the size of a small premium coffee—you'll have absorbed 187.5 mg after 28 minutes. You'll probably remember more from that morning meeting, too. Caffeine activates your sympathetic nervous system and stimulates the release of norepinepherine, a stress hormone that helps raise blood pressure and increase heart rate. The chemical sends a signal to your brain that something important is happening, and prompts your brain to store that information for later, explains Michael Yassa, Ph.D., a professor of psychological and brain sciences at Johns Hopkins University. When's the right time for your a.m. jolt? Yassa recently conducted a study that showed that consuming 200 mg of caffeine five minutes after learning something enhanced memory for at least 24 hours.
                  Enhance performance with a small caffeine boost an hour before your workout
                  Caffeine has been shown to improve muscle strength and endurance, which is why you see runners downing coffee before a race. You'll see this benefit by consuming low-to-moderate doses of caffeine—considered 3 to 6 mg per kg of body weight—15 minutes to one hour prior to exercise, according to a review paper in the Journal of the International Society of Sports Nutrition. If you're a 170-pound guy, that's between 213 and 463 mg of caffeine—roughly the amount found in a tall and venti Starbucks coffee, respectively. But cap it there: Downing higher amounts will not have you running like a track-and-field star. You'll only increase your risk of suffering caffeine side effects, like the shakes.
                  Quit at noon
                  Think you can brew a pot in the p.m. and still be fine? You may be duping yourself. When normal sleepers were given 400 mg of caffeine before bed—either six hours, three hours, or immediately before—all of them saw their sleep suffer, a small study in the Journal of Clinical Sleep Medicine found. Strangely enough, these drinkers were unable to detect the sleep-wrecking effects of caffeine, possibly because nighttime awakenings can be difficult to notice, says study author Christopher Drake, Ph.D. Cut yourself off at least 6 hours before bed.",
        'featured_image' => 'featured_image4.jpg',
        'thumbnail_image' => 'thumbnail_image4.jpg',
        'created_at' => Carbon\Carbon::now()->subDays(6),
        'updated_at' => Carbon\Carbon::now()->subDays(5)
      ]);
        
        //post id = 5
        DB::table('posts')->insert([
        'title' => "A coffee could reset your body clock and cheat jet lag",
        'excerpt' => "Drinking coffee in the evening can turn back the body clock and could help fight jet lag – but only for travellers heading west, scientists believe.",
        'user_id' => 1,
        'body' => "Coffee appears to trick the body into thinking that it is around an hour earlier in the day, reactiviating bodily functions that should be powering down in the evening.
                  For people travelling west on a plane who need to push back their body clocks, drinking a double espresso when changing time zones is likely to help with jet lag, said scientists from the Medical Research Council in Cambridge and the University of Colorado. But those travelling east may make jet lag worse by drinking coffee.
                  Dr John O'Neill, the joint lead researcher at the council's Laboratory of Molecular Biology, said: 'These findings could have important implications for people with circadian sleep disorders, where their normal 24-hour body clock doesn't work properly. 'Our findings also provide a more complete explanation for why it's harder for some people to sleep if they've had a coffee in the evening.'
                  The researchers showed that caffeine affects the body clock by delaying a rise in the level of melatonin, the main sleep hormone released by the body. The US team studied five people to see when melatonin started to appear in saliva. Each person lived in the lab for 49 days without a clock or external light to tell them if it was night or day.
                  They were then given caffeine, the equivalent of a double espresso, or a placebo three hours before they went to sleep and were exposed to dim or bright light – which also delays the human circadian clock – to find out when the surge in melatonin occurred. In those who were given the caffeine, their melatonin levels rose on average 40 minutes later than those given the placebo.",
        'featured_image' => 'featured_image5.jpg',
        'thumbnail_image' => 'thumbnail_image5.jpg',
        'created_at' => Carbon\Carbon::now(), //current date
        'updated_at' => Carbon\Carbon::now()
      ]);
        
        
      //create 300 fake posts using factory (PostFactory.php)
      //factory(\App\Post::class, 300)->create();
    }
}

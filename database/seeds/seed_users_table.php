<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class seed_users_table extends Seeder
{
  
  
  //php artisan db:seed --class=seed_users_table
  // to seed only 1 specific table
  
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //insert first user - admin user
      DB::table('users')->insert([
        'name' => 'userOne',
        'email' => 'user1@gmail.com',
        'password' => bcrypt('zzzzZ!1'),
        'isAdmin' => 1,
        'remember_token' => str_random(10),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);
      
      
      //insert second user - non-admin user
      DB::table('users')->insert([
        'name' => 'userTwo',
        'email' => 'user2@gmail.com',
        'password' => bcrypt('zzzzZ!1'),
        'isAdmin' => 0,
        'remember_token' => str_random(10),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);
      
      //then create 10 users auto
      //factory(\App\User::class, 10)->create();
    }
}

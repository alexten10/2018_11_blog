<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class seed_categories_table extends Seeder
{
  
  //php artisan db:seed --class=seed_categories_table
  // to seed only 1 specific table
  
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert(
        [
          'name' => 'coffee',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
        ]
      );
      
      DB::table('categories')->insert(
        [
          'name' => 'health',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
        ]
      );
      
      
      DB::table('categories')->insert(
        [
          'name' => 'lifestyle',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
        ]
      );
      
      
      DB::table('categories')->insert(
        [
          'name' => 'research',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
        ]
      );
      
      
      DB::table('categories')->insert(
        [
          'name' => 'benefit',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
        ]
      );
      
      
    }
}
